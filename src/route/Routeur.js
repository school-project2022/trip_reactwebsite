import React from 'react'
import { Routes, Route } from 'react-router-dom'

import '../css/style.css'

import LayoutNav from '../layout/navbar/layoutNav'

import Home from '../pages/Home'
import Faq from '../pages/faq'
import NotFound from './NotFound'

function Routeur() {
    return (
        <LayoutNav>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/faq" element={<Faq />} />
                <Route path="*" element={<NotFound />} />
            </Routes>
        </LayoutNav>
    )
}

export default Routeur
