import Header from '../../components/header'
import Footer from '../../components/footer'

const LayoutNav = (props) => {
    return (
        <>
            <Header />
            {props.children}
            <Footer />
        </>
    )
}

export default LayoutNav
