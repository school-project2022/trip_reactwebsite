// A executer seulement avec NodeJS

const { faker } = require('@faker-js/faker')
var fs = require('fs')

const datas = []

// choisir le nombre d'éléments à générer
const nbDatas = 30

for (let i = 0; i < nbDatas; i++) {
    let description = faker.lorem.paragraph()
    let image = faker.image.city(495, 330, true)
    let region = faker.address.cityName()
    let country = faker.address.country()
    let type = ['Particulier', 'Professionnel']
    let date = faker.date.future(1).toString()

    // par défaut faker.js place la devise devant le prix
    let prix_faker = faker.commerce.price(50, 1000, 0, '€').split('')

    // reverse premier element (devise) au dernier element
    prix_faker.push(prix_faker.shift())
    let prix = prix_faker.join('')

    datas.push({
        title: description,
        image: image,
        region: region,
        pays: country,
        type: type[Math.floor(Math.random() * type.length)],
        date: date,
        prix: prix,
    })
}

var dictString = JSON.stringify(datas)
fs.writeFile('./src/data/accueil/fakerData.json', dictString, function (err, result) {
    if (err) console.log('error', err)
})
