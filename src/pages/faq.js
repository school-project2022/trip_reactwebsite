import datas from '../data/faq/data.json'
import FAQ from '../components/FAQ'

function Faq() {
    return (
        <>
            <h1 className="faq-title">FAQ</h1>
            <div className="faq-questions">
                {datas.map((data, k) => (
                    <FAQ key={k} number={k + 1} question={data.question} answer={data.answer}></FAQ>
                ))}
            </div>
        </>
    )
}

export default Faq
