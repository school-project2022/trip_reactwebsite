import Country from '../components/country'
import { useState } from 'react'

// donnée aléatoirement généré avec faker.js
import datasJSON from '../data/accueil/fakerData.json'

function Home() {
    const datas = datasJSON
    const [foundDatas, setFoundDatas] = useState(datas)

    // Filtre par région par défaut
    const [filter, setFilter] = useState('region')

    // Retourne la liste des datas qui matchent avec au moins un caractère/chiffre saisie par l'utilisateur
    // Si le champ est vide, alors on affiche tout (comportement par défaut)
    const search = (e) => {
        const input = e.target.value.trim()

        if (input !== '') {
            const results = datas.filter((data) => {
                if (filter === 'pays') {
                    return data.pays.toLowerCase().startsWith(input.toLowerCase())
                } else if (filter === 'prix') {
                    // on retire le symbole de la devise afin de parser le prix en int et ensuite le comparer avec la saisie de l'user
                    return parseInt(data.prix.replace('€', '')) <= input
                }

                // Si aucun ou autre filtre que mentionné dans la condition, alors filtre par région
                return data.region.toLowerCase().startsWith(input.toLowerCase())
            })

            setFoundDatas(results)
        } else {
            setFoundDatas(datas)
        }
    }

    return (
        <>
            <div className="centerInput">
                <select onChange={(e) => setFilter(e.target.value)}>
                    <option value="region">Rechercher par région</option>
                    <option value="pays">Rechercher par pays</option>
                    <option value="prix">Filtrer par prix (maximum)</option>
                </select>
                <input
                    type={filter === 'prix' ? 'number' : 'search'}
                    className="searchInput"
                    onChange={search}
                    placeholder={
                        filter === 'prix' ? 'Entrez une valeur maximum' : 'Entrez une saisie'
                    }
                    autoFocus
                />
            </div>

            <div className="MyDiv">
                {foundDatas && foundDatas.length > 0 ? (
                    foundDatas.map((data, k) => (
                        <Country
                            title={data.title}
                            image={data.image}
                            region={data.region}
                            pays={data.pays}
                            type={data.type}
                            date={data.date}
                            prix={data.prix}
                            key={k}
                        />
                    ))
                ) : (
                    <h1>Aucun résultat trouvé.</h1>
                )}
            </div>
        </>
    )
}

export default Home
