const Footer = () => {
    return (
        <footer>
            <p>&copy; Projet réalisé par Laura TCHITOMBI et Carellien RAKOTOARISOA</p>
        </footer>
    )
}

export default Footer
