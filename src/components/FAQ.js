import React, { useState } from 'react'

const FAQ = ({ number, question, answer }) => {
    const [displayAnswer, setDisplayAnswer] = useState(false)

    return (
        <div className="faq-question" onClick={() => setDisplayAnswer(true)}>
            <div className="faq-question-header">
                <span className="faq-question-number">{number}</span>
                <span className="faq-question-askQuestion">{question}</span>
            </div>

            <div
                className={
                    displayAnswer ? 'faq-question-answer display' : 'faq-question-answer hide'
                }
            >
                {answer}
            </div>
        </div>
    )
}

export default FAQ
