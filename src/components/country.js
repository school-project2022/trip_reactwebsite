import React from 'react'

const country = ({ title, image, region, pays, type, date, prix }) => {
    return (
        <div className="country">
            <img src={image} alt={title} />
            <h2>
                {region}, {pays}
            </h2>
            <p>{type}</p>
            <p>{date}</p>
            <h2>{prix} / nuit</h2>
        </div>
    )
}

export default country
