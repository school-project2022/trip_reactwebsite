import { Link } from 'react-router-dom'
import Nav from '../hooks/nav'

const Header = () => {
    return (
        <header>
            <Nav>
                <div className="container-links">
                    <Link to="/" className="link">
                        Accueil
                    </Link>
                    <Link to="/faq" className="link">
                        FAQ
                    </Link>
                </div>
            </Nav>
        </header>
    )
}

export default Header
