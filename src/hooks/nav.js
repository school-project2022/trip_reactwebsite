import React, { useState, useEffect } from 'react'

export default function Nav(props) {
    const [scroll, setScroll] = useState(false)

    useEffect(() => {
        window.addEventListener('scroll', () => {
            setScroll(window.scrollY > 50)
        })
    }, [])

    return (
        <>
            <nav className={scroll ? 'nav scrolling' : 'nav top'}>{props.children}</nav>
        </>
    )
}
